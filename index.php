<?php

session_start();

require_once("vendor/autoload.php");

use \Slim\Slim;

$app = new Slim();

$app->config('debug', true);

require_once "functions.php";
require_once "t-site.php";
require_once "t-admin.php";
require_once "t-admin-users.php";
require_once "t-admin-categories.php";
require_once "t-admin-products.php";

$app->run();

?>