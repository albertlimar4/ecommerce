<?php

use \Hcode\Page;
use \Hcode\Model\User;
use \Hcode\Model\Category;
use \Hcode\Model\Product;
use \Hcode\Model\Cart;
use \Hcode\Model\Address;

$app->get('/', function() {

    $products = Product::listAll();

    $page = new Page();

    $page->setTPL("index", [
        'products'=>Product::checkList($products)
    ]);

});

$app->get('/categorias', function() {

    User::verifyLogin();

    $page = new Page();

    $page->setTpl("category");

});

$app->get('/categorias/:idcategory', function($idcategory) {

    $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

    $category = new Category();

    $category->get((int)$idcategory);

    $pagination = $category->getProductsPage($page);

    $pages = [];

    for ($i=1; $i <= $pagination['pages']; $i++){
        array_push($pages, [
            'link'=>'/categorias/'.$category->getidcategory().'?page='.$i,
            'page'=>$i
        ]);
    }

    $page = new Page();

    $page->setTpl("category", [
        "category"=>$category->getValues(),
        "products"=>$pagination['data'],
        "pages"=>$pages
    ]);

});

$app->get('/produtos/:desurl', function($desurl) {

    $produto = new Product();

    $produto->getFromUrl($desurl);

    $page = new Page();

    $page->setTpl("product-detail", array(
        'produto'=>$produto->getValues(),
        'categorias'=>$produto->getCategories()
    ));

});

$app->get('/cart', function() {

    $cart = Cart::getFromSession();

    $page = new Page();

    $page->setTpl("cart", [
        'cart'=>$cart->getValues(),
        'products'=>$cart->getProducts(),
        'error'=>Cart::getMsgError()
    ]);

});

$app->get('/cart/:idproduct/add', function($idproduct) {

    $product = new Product();

    $product->get((int)$idproduct);

    $cart = Cart::getFromSession();

    $qtd = (isset($_GET['qtd'])) ? (int)$_GET['qtd'] : 1;

    for ($i = 0;$i < $qtd;$i++){
        $cart->addProduct($product);
    }

    header("Location: /cart");
    exit();

});

$app->get('/cart/:idproduct/minus', function($idproduct) {

    $product = new Product();

    $product->get((int)$idproduct);

    $cart = Cart::getFromSession();

    $cart->removeProduct($product);

    header("Location: /cart");
    exit();

});

$app->get('/cart/:idproduct/remove', function($idproduct) {

    $product = new Product();

    $product->get((int)$idproduct);

    $cart = Cart::getFromSession();

    $cart->removeProduct($product, true);

    header("Location: /cart");
    exit();

});

$app->post('/cart/freight', function() {

    $cart = Cart::getFromSession();

    $cart->setFreight($_POST['zipcode']);

    header("Location: /cart");
    exit();

});

$app->get('/checkout', function() {

    User::verifyLogin(false);

    $cart = Cart::getFromSession();

    $address = new Address();

    $page = new Page();

    $page->setTpl("checkout", [
        'cart'=>$cart->getValues(),
        'address'=>$address->getValues()
    ]);

});

$app->get('/login', function() {

    $page = new Page();

    $page->setTpl("login", [
        "errorRegister"=>User::getErrorRegister(),
        "error"=>User::getError(),
        "registerValues"=>(isset($_SESSION['registerValues'])) ? $_SESSION['registerValues'] : ['name'=>'', 'email'=>'', 'phone'=>'']
    ]);

});

$app->post('/login', function() {

    try{
        User::login($_POST['login'], $_POST['password']);
    } catch (Exception $e) {

        User::setError($e->getMessage());

    }

    header("Location: /checkout");
    exit();

});

$app->get('/logout', function() {

    User::logout();

    header("Location: /login");
    exit();

});

$app->post('/register', function() {

    $_SESSION['registerValues'] = $_POST;

    if (!isset($_POST['name']) || $_POST['name'] == ''){

        User::setErrorRegister("Preencha seu nome corretamente.");

        header("Location: /login");
        exit();
    }

    if (!isset($_POST['email']) || $_POST['email'] == ''){

        User::setErrorRegister("Preencha seu email corretamente.");

        header("Location: /login");
        exit();
    }

    if (!isset($_POST['password']) || $_POST['password'] == ''){

        User::setErrorRegister("Preencha sua senha corretamente.");

        header("Location: /login");
        exit();
    }

    if (User::checkLoginExist($_POST['email']) === true){

        User::setErrorRegister("Email já cadastrado.");

        header("Location: /login");
        exit();
    }

    $user = new User();

    $user->setData([
        'inadmin'=>0,
        'deslogin'=>$_POST['email'],
        'desperson'=>$_POST['name'],
        'desemail'=>$_POST['email'],
        'despassword'=>$_POST['password'],
        'nrphone'=>$_POST['phone']
    ]);

    $user->save();

    User::login($_POST['email'], $_POST['password']);

    header("Location: /checkout");
    exit();

});

$app->get('/forgot', function() {

    $page = new Page;

    $page->setTpl("forgot");

});

$app->post('/forgot', function() {

    $user = new User();

    $user = User::getForgot($_POST['email'], false);

    header("Location: /forgot/sent");
    exit();

});

$app->get("/forgot/sent", function (){

    $page = new Page;

    $page->setTpl("forgot-sent");

});

$app->get("/forgot/reset", function (){

    $user = User::validforgotDecrypt($_GET['code']);

    $page = new Page;

    $page->setTpl("forgot-reset", array(
        "name"=>$user['desperson'],
        "code"=>$_GET['code']
    ));

});

$app->post("/forgot/reset", function (){

    $forgot = User::validforgotDecrypt($_POST['code']);

    User::setFogotUser($_POST['code']);

    $user = new User();

    $user->get((int)$forgot['iduser']);

    $password = password_hash($_POST['password'], PASSWORD_DEFAULT, [
        "cost"=>12
    ]);

    $user->setPassaword($password);

    $page = new Page;

    $page->setTpl("forgot-reset-success");

});

$app->get("/profile", function (){

    User::verifyLogin(false);

    $user = User::getFromSession();

    $page = new Page;

    $page->setTpl("profile", [
        "user"=>$user->getValues(),
        "profileMsg"=>User::getSuccess(),
        "profileError"=>User::getError()
    ]);

});

$app->post('/profile', function() {

    User::verifyLogin(false);

    if (!isset($_POST['desperson']) || $_POST['desperson'] == ''){

        User::setError("Preencha seu nome corretamente.");

        header("Location: /profile");
        exit();
    }

    if (!isset($_POST['desemail']) || $_POST['desemail'] == ''){

        User::setError("Preencha seu email corretamente.");

        header("Location: /profile");
        exit();
    }

    $user = User::getFromSession();

    if ($_POST['desemail'] !== $user->getdesemail()){

        if (User::checkLoginExist($_POST['desemail'])){

            User::setError("Email já cadastrado.");

            header("Location: /profile");
            exit();
        }

    }

    $_POST['inadmin'] = $user->getinadmin();
    $_POST['despassword'] = $user->getdespassword();
    $_POST['deslogin'] = $_POST['desemail'];

    $user->setData($_POST);

    $user->update();

    User::getSuccess("Dados alterados com sucesso");

    header("Location: /profile");
    exit();

});